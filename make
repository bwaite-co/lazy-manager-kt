#!/bin/bash

if [ ! -f one.txt ]; then
    echo one >> one.txt
fi
if [ ! -f two.txt ]; then
    echo two >> two.txt
fi

set +x

kotlinc src/*.kt

python -m SimpleHTTPServer &
PID=$!

kotlin lazy.MainKt

kill -2 ${PID}
