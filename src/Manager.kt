package lazy

import java.net.URL
import java.util.Scanner

class Manager(filename: String) {
    val get: String by lazy { 
        val res = URL("""http://localhost:8000/${filename}""").openConnection().getInputStream()

        val scan = Scanner(res)
        scan.useDelimiter("\\A").next()
    }
}

