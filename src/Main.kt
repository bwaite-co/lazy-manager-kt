package lazy

import java.net.URL
import java.util.Scanner

fun main(args: Array<String>) {
    val one = Manager("one.txt")
    val two = Manager("two.txt")

    println(one.get)
    println(one.get)
    println(two.get)
}
