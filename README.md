Example of using a data manager with kotlin lazy properties to not double request a resource during the lifetime of a data manger.

Run to see example output
```bash
./make
```